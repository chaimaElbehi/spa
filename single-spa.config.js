import {registerApplication, start} from 'single-spa'

import {loadEmberApp} from 'single-spa-ember';



// single-spa-ember helps us load the script tags and give the ember app module to single-spa.
function loadingFunction() {
    const appName = 'ember-app';
    
    const appUrl ='./src/ember-app/dist/assets/ember-app.js';
    const vendorUrl = './src/ember-app/dist/assets/vendor.js'; // Optional if you have one vendor bundle used for many different ember apps
    return loadEmberApp(appName, appUrl ,vendorUrl);
  }
registerApplication(
  // Name of our single-spa application
  'home',
  // Our loading function:function that returns a promise 
  () => import('./src/home/home.app.js'),  
  () => location.pathname.startsWith('/home') // Our activity function
);
  


    registerApplication(
        'angularjs', 
        () => import ('./src/angularJS/angularJS.app.js'), 
        () => location.pathname.startsWith('/angularjs')
        );
        registerApplication(  
            'navBar',   
            () => import('./src/home/navBar/navBar.app.js').then(module => module.navBar),
             //we want our application to be always shown thhat's why 
            //we return the navBar application as property cuase import return an application config obj 
            () => true
        );
        registerApplication(
            'vue',
            ()=>import('../single-spa-simple-example/src/vue/vue.app'),
            ()=>location.pathname.startsWith('/vue')
        );

         registerApplication(
            'ember',
            loadingFunction(),
            ()=>location.pathname.startsWith('/ember')
        );
               
start()