import singleSpaAngularJS from 'single-spa-angularjs';
import angular from 'angular';
import './root.component';

/*const domElementGetter = () => {
  let el = document.getElementById("angularjs")
  if (!el) {
    el = document.createElement('div')
    el.id = "angularjs"
    document.body.appendChild(el)
  }

  return el
}
*/

const domElementGetter = () => document.getElementById("angularjs");
console.log('here the dom',domElementGetter)
const angularLifecycles = singleSpaAngularJS({
  angular,
  domElementGetter,
  mainAngularModule: 'app',
  uiRouter: false,
  preserveGlobal: false,
})
//lifeCycle of the application
export const mount = [
  angularLifecycles.mount,
  mountdd,
];
export const bootstrap = [
  angularLifecycles.bootstrap,
  boostrappedFn,
];
export const unmount = [
  angularLifecycles.unmount,
  unmountfn,
];

///////////functions/////////////////////////////
function boostrappedFn(props) {
  angularLifecycles.bootstrap;
  return Promise
    .resolve()
    .then(() => {
      console.log('boostrapped angular JS!');
    });
}

function mountdd(props) {
  angularLifecycles.mount;

  return Promise
    .resolve()
    .then(() => {
      // This is where you tell a framework (e.g., React) to render some ui from the dom
      console.log('mounted angular JS!');
    });
}



function unmountfn(props) {
  angularLifecycles.unmount;

  return Promise
    .resolve()
    .then(() => {
      // This is where you tell a framework (e.g., React) to unrender some ui from the dom
      console.log('unmounted angularjs!');
    });
}


