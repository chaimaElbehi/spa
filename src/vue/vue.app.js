import Vue from 'vue/dist/vue.min.js';
import singleSpaVue from 'single-spa-vue';
import './vue.styles.css'

const vueLifecycles = singleSpaVue({
  Vue,
  appOptions: {
    el: '#vue-app',
    /* if u have  routing inside the vue ,you should spedcify it here in order to work  like bellow:
    appOptions: {
    render: h => h(App),
    router,
  }*/
 
    template: `
    <div  id="vue-app" style="width: 50%; margin: 0px auto;"  class="card">
    <div class="container">
      <h4><b>Vue framework</b></h4> 
      <p>Integration</p> 
    </div>
  </div>
    `
    
    
   
  }
});

export const bootstrap = [
  vueLifecycles.bootstrap,
 boostrappedFn
];

export const mount = [
  vueLifecycles.mount,
  mountdd
];

export const unmount = [
  vueLifecycles.unmount,
  unmountfn
];
//////////functions/////////////////////////////
function boostrappedFn(props) {
  return Promise
    .resolve()
    .then(() => {
      console.log('boostrapped vue!');
    });
}

function mountdd(props) {
  return Promise
    .resolve()
    .then(() => {
      // This is where you tell a framework (e.g., React) to render some ui from the dom
      console.log('mounted vue!');
    });
}



function unmountfn(props) {
  return Promise
    .resolve()
    .then(() => {
      // This is where you tell a framework (e.g., React) to unrender some ui from the dom
      console.log('unmounted vue!');
    });
  }
