import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import angular from '../angularJS/root.component';


function App() {
  return (
    <Router>
      <div>

        <Route exact path="/angulartt" component={angular} />
      </div>
    </Router>
  );
}