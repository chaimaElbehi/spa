import React from "react";
import {navigateToUrl} from 'single-spa';
import e from '../eventBus';

export default class Root extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      message: 'When Angular receives message, we should see a confirmation here'
    }

    this.messageHandler = this.messageHandler.bind(this)
  }

  componentDidMount() {
    e.on('received', this.messageHandler)
  }

  componentDidUnmount() {
    e.off('received', this.messageHandler)
  }

  messageHandler(message) {
    this.setState({
      message: message.text
    })
  }

  sendMessage() {
    e.emit('message', { text: 'Hello from React' })
  }


  render() {
    return (
      <div style={{marginTop: '10px'}}>
       <h1>This is written in React</h1>
      <div class="vertical-menu">
  <a href='/angularjs' onClick={navigateToUrl} class='active'>AngularJS home component(react)</a>
  <a href='/'onClick={navigateToUrl} >go to home (react component)</a>
  <button onClick={this.sendMessage}>
            Send a message to menubar 
      </button>
  </div>
  <p>{this.state.message}</p>
      </div>
      
    )
  }
}



