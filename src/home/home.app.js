import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import Home from './root.component.js';
import './home.css';
function domElementGetter() {
  return document.getElementById("home")
}

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Home,
  domElementGetter,
})
export const bootstrap = [
  reactLifecycles.bootstrap,
  boostrappedFn
];

export const mount = [
  reactLifecycles.mount,
  mountdd
];

export const unmount = [
  reactLifecycles.unmount,
  unmountfn

];



//////////functions/////////////////////////////
function boostrappedFn(props) {
  return Promise
    .resolve()
    .then(() => {
      console.log('boostrapped react sidebar!');
    });
}

function mountdd(props) {
  return Promise
    .resolve()
    .then(() => {
      // This is where you tell a framework (e.g., React) to render some ui from the dom
      console.log('mounted react sidebar');
    });
}



function unmountfn(props) {
  return Promise
    .resolve()
    .then(() => {
      // This is where you tell a framework (e.g., React) to unrender some ui from the dom
      console.log('unmounted react sidebar!');
    });
  }

