import React from 'react'
import {navigateToUrl} from 'single-spa'
const NavBar = () => (
  <nav>
    <div className="nav-wrapper">
      <a href="/" className="brand-logo">single-spa</a>      
      <ul id="nav-mobile" className="right hide-on-med-and-down">
        <li><a href="/home" onClick={navigateToUrl} feed='hello from navBar hitting home to send this message'>vertical menu</a></li>     
  
        <li><a href="/angularjs" onClick={navigateToUrl}>AngularJS</a></li>     
        <li><a href="/vue" onClick={navigateToUrl} feed='hello from navBar hitting home to send this message'>Vue</a></li> 
        <li><a href="/ember" onClick={navigateToUrl} >ember</a></li>  
      </ul>
    </div>
  </nav>
)

export default NavBar